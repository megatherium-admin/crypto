/*global __dirname,Buffer,describe,it*/
import {
	assert,
	proxyquire,
} from '@megatherium/test';
import {
	getRandomString,
} from '@megatherium/random';
import logger from './logger';
import path from 'path';

const log = logger.init('decrypt.js');

const stubs = {},
	encrypt = proxyquire(path.join(__dirname, '..', '..', 'lib', 'encrypt.js'), stubs).default,
	key = getRandomString(32);

let key2 = getRandomString(32);
while (key2 === key) {
	key2 = getRandomString(32);
}

describe(`lib/encrypt`, () => {
	it(`should be a function`, () => {
		assert.typeOf(encrypt, 'function');
	});

	describe('Booleans', () => {
		// it(`should return the same encryption if given the same boolean`, async() => {
		// 	const plain = true,
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different booleans`, async() => {
			const plain = true,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, !plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, !plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = true,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Buffers', () => {
		// it(`should return the same encryption if given the same buffer`, async() => {
		// 	const plain = Buffer.from(key),
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different buffers`, async() => {
			const plain = Buffer.from(key),
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, Buffer.from(key + '2')),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, Buffer.from(key + '2'));
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = Buffer.from(key),
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Functions', () => {
		// it(`should return the same encryption if given the same function`, async() => {
		// 	const plain = () => {
		// 			console.log('Hello, World!');
		// 		},
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different functions`, async() => {
			const plain = () => {
					log.debug('Hello, World!');
				},
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, () => {
						log.debug('Hello, World2!');
					}),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, () => {
					log.debug('Hello, World2!');
				});
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = () => {
					log.debug('Hello, World!');
				},
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Integers', () => {
		// it(`should return the same encryption if given the same integer`, async() => {
		// 	const plain = 1337,
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different integers`, async() => {
			const plain = 1337,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, plain + 1),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, plain + 1);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = 1337,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Null and undefined', () => {
		// it(`should return the same encryption if given the same null`, async() => {
		// 	const plain = null,
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given null and undefined`, async() => {
			const plain = null,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, undefined),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, undefined);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions with null if given different keys`, async() => {
			const plain = null,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions with undefined if given different keys`, async() => {
			const plain = undefined,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Numbers', () => {
		// it(`should return the same encryption if given the same number`, async() => {
		// 	const plain = 1.23456,
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different numbers`, async() => {
			const plain = 1.23456,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, plain + 0.000001),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, plain + 0.000001);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = 1.23456,
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Objects', () => {
		// it(`should return the same encryption if given the same object`, async() => {
		// 	const plain = {test: true},
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different objects`, async() => {
			const plain = {
					test: true,
				},
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, {
						...plain,
						test2: false,
					}),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, {
					...plain,
					test2: false,
				});
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = {
					test: true,
				},
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});

	describe('Strings', () => {
		// it(`should return the same encryption if given the same string`, async() => {
		// 	const plain = 'test-plain',
		// 		encryptions = [
		// 			await encrypt(key, plain),
		// 			await encrypt(key, plain),
		// 		];
		// 	encryptions.forEach(encryption => {
		// 		assert.unequal(encryption, plain);
		// 		encryptions.forEach(encryption2 => {
		// 			assert.equal(encryption, encryption2);
		// 		});
		// 	});
		// });

		it(`should be a different encryption if given different strings`, async() => {
			const plain = 'test-plain',
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key, plain + '2'),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				assert.unequal(encryption, plain + '2');
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}
					assert.unequal(encryption, encryption2);
				});
			});
		});

		it(`should return different encryptions if given different keys`, async() => {
			const plain = 'test-plain',
				encryptions = [
					await encrypt(key, plain),
					await encrypt(key2, plain),
				];
			encryptions.forEach((encryption, encryptionIndex) => {
				assert.unequal(encryption, plain);
				encryptions.forEach((encryption2, encryptionIndex2) => {
					if (encryptionIndex === encryptionIndex2) {
						return;
					}

					assert.unequal(encryption, encryption2);
				});
			});
		});
	});
});
