/*global __dirname,Buffer,describe,it*/
import {
	assert,
	proxyquire,
} from '@megatherium/test';
import logger from './logger';
import path from 'path';

const log = logger.init('sha1.js');

const stubs = {},
	sha1 = proxyquire(path.join(__dirname, '..', '..', 'lib', 'sha1.js'), stubs).default;

describe(`lib/sha1`, () => {
	it(`should be a function`, () => {
		assert.typeOf(sha1, 'function');
	});

	describe('Booleans', () => {
		it(`should return the same sha1 if given the same boolean`, async() => {
			const plain = true,
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different booleans`, async() => {
			const plain = true,
				checksums = [
					await sha1(plain),
					await sha1(!plain),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, !plain);
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Buffers', () => {
		it(`should return the same sha1 if given the same buffer`, async() => {
			const plain = Buffer.from('abcdefgh'),
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different buffers`, async() => {
			const plain = Buffer.from('abcdefgh'),
				checksums = [
					await sha1(plain),
					await sha1(Buffer.from('abcdefghi')),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, Buffer.from('abcdefghi'));
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Functions', () => {
		it(`should return the same sha1 if given the same function`, async() => {
			const plain = () => {
					log.debug('Hello, World!');
				},
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different functions`, async() => {
			const plain = () => {
					log.debug('Hello, World!');
				},
				checksums = [
					await sha1(plain),
					await sha1(() => {
						log.debug('Hello, World2!');
					}),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, () => {
					log.debug('Hello, World2!');
				});
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Integers', () => {
		it(`should return the same sha1 if given the same integer`, async() => {
			const plain = 1337,
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different integers`, async() => {
			const plain = 1337,
				checksums = [
					await sha1(plain),
					await sha1(plain + 1),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, plain + 1);
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Null and undefined', () => {
		it(`should return the same sha1 if given the same null`, async() => {
			const plain = null,
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given null and undefined`, async() => {
			const plain = null,
				checksums = [
					await sha1(plain),
					await sha1(undefined),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, undefined);
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Numbers', () => {
		it(`should return the same sha1 if given the same number`, async() => {
			const plain = 1.23456,
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different numbers`, async() => {
			const plain = 1.23456,
				checksums = [
					await sha1(plain),
					await sha1(plain + 0.000001),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, plain + 0.000001);
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Objects', () => {
		it(`should return the same sha1 if given the same object`, async() => {
			const plain = {
					test: true,
				},
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different objects`, async() => {
			const plain = {
					test: true,
				},
				checksums = [
					await sha1(plain),
					await sha1({
						...plain,
						test2: false,
					}),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, {
					...plain,
					test2: false,
				});
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});

	describe('Strings', () => {
		it(`should return the same sha1 if given the same string`, async() => {
			const plain = 'test-plain',
				checksums = [
					await sha1(plain),
					await sha1(plain),
				];
			checksums.forEach(sha1 => {
				assert.unequal(sha1, plain);
				checksums.forEach(sha12 => {
					assert.equal(sha1, sha12);
				});
			});
		});

		it(`should be a different sha1 if given different strings`, async() => {
			const plain = 'test-plain',
				checksums = [
					await sha1(plain),
					await sha1(plain + '2'),
				];
			checksums.forEach((sha1, sha1Index) => {
				assert.unequal(sha1, plain);
				assert.unequal(sha1, plain + '2');
				checksums.forEach((sha12, sha1Index2) => {
					if (sha1Index === sha1Index2) {
						return;
					}
					assert.unequal(sha1, sha12);
				});
			});
		});
	});
});
