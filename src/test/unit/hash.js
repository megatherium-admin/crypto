/*global __dirname,Buffer,describe,it*/
import {
	assert,
	proxyquire,
} from '@megatherium/test';
import logger from './logger';
import path from 'path';

const log = logger.init('hash.js');

const stubs = {},
	hash = proxyquire(path.join(__dirname, '..', '..', 'lib', 'hash.js'), stubs).default;

describe(`lib/hash`, () => {
	it(`should be a function`, () => {
		assert.typeOf(hash, 'function');
	});

	describe('Booleans', () => {
		it(`should return the same hash if given the same boolean`, async() => {
			const plain = true,
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different booleans`, async() => {
			const plain = true,
				hashes = [
					await hash(plain),
					await hash(!plain),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, !plain);
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Buffers', () => {
		it(`should return the same hash if given the same buffer`, async() => {
			const plain = Buffer.from('abcdefgh'),
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different buffers`, async() => {
			const plain = Buffer.from('abcdefgh'),
				hashes = [
					await hash(plain),
					await hash(Buffer.from('abcdefghi')),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, Buffer.from('abcdefghi'));
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Functions', () => {
		it(`should return the same hash if given the same function`, async() => {
			const plain = () => {
					log.debug('Hello, World!');
				},
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different functions`, async() => {
			const plain = () => {
					log.debug('Hello, World!');
				},
				hashes = [
					await hash(plain),
					await hash(() => {
						log.debug('Hello, World2!');
					}),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, () => {
					log.debug('Hello, World2!');
				});
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Integers', () => {
		it(`should return the same hash if given the same integer`, async() => {
			const plain = 1337,
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different integers`, async() => {
			const plain = 1337,
				hashes = [
					await hash(plain),
					await hash(plain + 1),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, plain + 1);
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Null and undefined', () => {
		it(`should return the same hash if given the same null`, async() => {
			const plain = null,
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given null and undefined`, async() => {
			const plain = null,
				hashes = [
					await hash(plain),
					await hash(undefined),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, undefined);
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Numbers', () => {
		it(`should return the same hash if given the same number`, async() => {
			const plain = 1.23456,
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different numbers`, async() => {
			const plain = 1.23456,
				hashes = [
					await hash(plain),
					await hash(plain + 0.000001),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, plain + 0.000001);
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Objects', () => {
		it(`should return the same hash if given the same object`, async() => {
			const plain = {
					test: true,
				},
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different objects`, async() => {
			const plain = {
					test: true,
				},
				hashes = [
					await hash(plain),
					await hash({
						...plain,
						test2: false,
					}),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, {
					...plain,
					test2: false,
				});
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					log.debug(`comparing`, {
						hashIndex,
						hashIndex2,
						result: hash !== hash2,
					});
					assert.unequal(hash, hash2);
				});
			});
		});
	});

	describe('Strings', () => {
		it(`should return the same hash if given the same string`, async() => {
			const plain = 'test-plain',
				hashes = [
					await hash(plain),
					await hash(plain),
				];
			hashes.forEach(hash => {
				assert.unequal(hash, plain);
				hashes.forEach(hash2 => {
					assert.equal(hash, hash2);
				});
			});
		});

		it(`should be a different hash if given different strings`, async() => {
			const plain = 'test-plain',
				hashes = [
					await hash(plain),
					await hash(plain + '2'),
				];
			hashes.forEach((hash, hashIndex) => {
				assert.unequal(hash, plain);
				assert.unequal(hash, plain + '2');
				hashes.forEach((hash2, hashIndex2) => {
					if (hashIndex === hashIndex2) {
						return;
					}
					assert.unequal(hash, hash2);
				});
			});
		});
	});
});
