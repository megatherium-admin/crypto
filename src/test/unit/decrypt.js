/*global __dirname,Buffer,describe,it*/
import {
	assert,
	proxyquire,
} from '@megatherium/test';
import {
	getRandomString,
} from '@megatherium/random';
import logger from './logger';
import path from 'path';

const log = logger.init('decrypt.js');

const stubs = {},
	convertToBuffer = proxyquire(path.join(__dirname, '..', '..', 'lib', 'convertToBuffer.js'), stubs).default,
	decrypt = proxyquire(path.join(__dirname, '..', '..', 'lib', 'decrypt.js'), stubs).default,
	encrypt = proxyquire(path.join(__dirname, '..', '..', 'lib', 'encrypt.js'), stubs).default,
	key = getRandomString(32);

let key2 = getRandomString(32);
while (key2 === key) {
	key2 = getRandomString(32);
}

describe(`lib/encrypt`, () => {
	it(`should be a function`, () => {
		assert.typeOf(encrypt, 'function');
	});

	describe('Booleans', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = true,
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = true,
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Buffers', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = Buffer.from(key),
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = Buffer.from(key),
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Functions', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = () => {
					log.debug(`Hello, World!`);
				},
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = () => {
						log.debug(`Hello, World!`);
					},
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Integers', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = 1337,
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = 1337,
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, convertToBuffer(plain));
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Null', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = null,
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = null,
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Undefined', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = undefined,
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = undefined,
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Numbers', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = 1.23456,
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = 1.23456,
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Objects', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = {
					test: true,
				},
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption, convertToBuffer(plain));
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = {
						test: true,
					},
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption, plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});

	describe('Strings', () => {
		it(`should return the plain text if decrypted with the same key`, async() => {
			const plain = 'test-plain',
				encryption = await encrypt(key, plain),
				decryption = await decrypt(key, encryption);
			assert.equal(decryption.toString('utf-8'), plain);
		});

		it(`should not return the plain text if decrypted with a different key`, async() => {
			let errThrown = false;
			try {
				const plain = 'test-plain',
					encryption = await encrypt(key, plain),
					decryption = await decrypt(key2, encryption);
				assert.unequal(decryption.toString('utf-8'), plain);
			} catch (err) {
				errThrown = true;
			}
			assert(errThrown);
		});
	});
});
