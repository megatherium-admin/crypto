import convertToBuffer from './convertToBuffer';
import crypto from 'crypto';
import logger from './logger';

const log = logger.init('hash.js');

/**
 * Creates a SHA512 checksum from a value.
 * @access public
 * @augments crypto
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create a checksum</caption>
 * import assert from 'assert';
 * import {
 * 	hash,
 * } from '@megatherium/crypto';
 *
 * (async() => {
 * 	const originalValue = true,
 * 		encryptedValue = await hash(originalValue);
 *
 * 	assert.equal(typeof encryptedValue, 'string');
 * 	assert.notEqual(encryptedValue, originalValue);
 * })();
 * @license Megatherium Standard License 1.0
 * @module hash
 * @param {any} value The value to create the checksum from.
 * @returns {Promise<string>} Returns the encrypted value as a hex-string.
 */
const hash = (value) => {
	const hash = crypto.createHash('sha512');
	hash.update(convertToBuffer(value));

	return hash.digest('hex');
};

export default hash;
