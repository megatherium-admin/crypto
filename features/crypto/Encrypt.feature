Feature: Sha1 Checksums

	Given the key "12345678901234567890123456789012"

	Scenario: Encrypting an integer
		Given a random integer
		When I encrypt it
		Then the encrypted should not contain it
	
	Scenario: Encrypting integers
		Given 2 random integers
		When I encrypt them
		Then the encrypted should not contain them
	
	Scenario: Encrypting a number
		Given a random number
		When I encrypt it
		Then the encrypted should not contain it
	
	Scenario: Encrypting numbers
		Given 2 random numbers
		When I encrypt them
		Then the encrypted should not contain them

	Scenario: Encrypting a string
		Given a random string
		When I encrypt it
		Then the encrypted should not contain it
	
	Scenario: Encrypting strings
		Given 2 random strings
		When I encrypt them
		Then the encrypted should not contain them