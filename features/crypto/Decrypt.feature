Feature: Sha1 Checksums

	Given the key "12345678901234567890123456789012"

	Scenario: Decrypting an integer
		Given a random integer
		When I encrypt it
		Then the encrypted should not contain it
		When I decrypt it
		Then the decrypted should contain it
	
	Scenario: Decrypting integers
		Given 2 random integers
		When I encrypt them
		Then the encrypted should not contain them
		When I decrypt them
		Then the decrypted should contain them
	
	Scenario: Decrypting a number
		Given a random number
		When I encrypt it
		Then the encrypted should not contain it
		When I decrypt it
		Then the decrypted should contain it
	
	Scenario: Decrypting numbers
		Given 2 random numbers
		When I encrypt them
		Then the encrypted should not contain them
		When I decrypt them
		Then the decrypted should contain them

	Scenario: Decrypting a string
		Given a random string
		When I encrypt it
		Then the encrypted should not contain it
		When I decrypt it
		Then the decrypted should contain it
	
	Scenario: Decrypting strings
		Given 2 random strings
		When I encrypt them
		Then the encrypted should not contain them
		When I decrypt them
		Then the decrypted should contain them